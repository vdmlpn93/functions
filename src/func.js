const getSum = (str1, str2) => {
  if (isNaN(str1) || isNaN(str2) || typeof (str1) === 'object' || typeof (str2) === 'object') 
      return false;
  
  return String(Number(str1)+Number(str2));
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let authorPosts = 0;
  let authorComments = 0;

  for (let p of listOfPosts)
  {
    if (p.author === authorName) authorPosts++;
    if ("comments" in p)
    {
      for (let com of p.comments)
      {
        if (com.author === authorName) authorComments++;
      }
    }
  }

  return `Post:${authorPosts},comments:${authorComments}`;
};

const tickets=(people)=> {
  let sum = 0;
  const cost = 25;
  for (let cash of people)
  {
    sum +=cost;
    if (sum< cash) return "NO"
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
